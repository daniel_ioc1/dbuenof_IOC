package com.mycompany.dbuenof_ioc;

import java.util.Scanner;

/**
 *
 * @author daniel.bueno
 */
public class Dbuenof_IOC {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in); // Creem un objecte Scanner per llegir l'entrada
        System.out.print("Com et dius? "); // Preguntem el nom
        String nom = scanner.nextLine(); // Llegim el nom

        // Saludem l'usuari utilitzant el nom que ha introduït
        System.out.println("Hola " + nom + ", soc el petit programa del Daniel Bueno Fernández, encantat!");
        System.out.println("He aprés molt fent el mòdul 8 de DAW encara que hagi estat dur!!!");
        scanner.close(); // Tanquem l'scanner
    }
}